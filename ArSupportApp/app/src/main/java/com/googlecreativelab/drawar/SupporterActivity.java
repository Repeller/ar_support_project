package com.googlecreativelab.drawar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SupporterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supporter);
    }
}